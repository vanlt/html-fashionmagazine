import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import Login from '../login/Login';
import Register from '../register/Register';
import Product from '../product/Product';

class RouterHeader extends React.Component{
  render(){
    return (
      <Router>
        <React.Fragment>
          <nav className="navbar navbar-default" role="navigation">
            
            <div className="navbar-header">
              <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              <a className="navbar-brand" href="#">Home</a>
            </div>
          
           
            <div className="collapse navbar-collapse navbar-ex1-collapse">
              <ul className="nav navbar-nav">
                <li className="active"><a href="#">Link</a></li>
                <li><Link to ="/product">Product</Link></li>
              </ul>
              <form className="navbar-form navbar-left" role="search">
                <div className="form-group">
                  <input type="text" className="form-control" placeholder="Search"/>
                </div>
                <button type="submit" className="btn btn-default">Submit</button>
              </form>
              <ul className="nav navbar-nav navbar-right">
                <li><Link to ="/login">Login</Link></li>
                <li><Link to ="/register">Register</Link></li>
              </ul>
            </div>
          </nav>
          <Route exact path ="/product" component={Product}/>
          <Route  path ="/login" component={Login}/>
          <Route  path ="/register" component={Register}/> 
       </React.Fragment>
       </Router>
    );
  }
  
}

export default RouterHeader;
 