import React from 'react';
import './App.css';
import Header from './components/header/header'

function App() {
  return (
    <React.Fragment>
      <Header/>
    </React.Fragment>
  );
}

export default App;
