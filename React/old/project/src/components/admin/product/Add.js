import React from 'react';

class Add extends React.Component {
    constructor(props){
        super(props);
      }
    render(){
        return (
            <div className="container">
            <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <form>
                <legend>Add New Product</legend>
                <div className="form-group">
                    <label>Name</label>
                    <input 
                        type="text" 
                        className="form-control"  
                        placeholder="Input field" />
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        placeholder="Input field" />
                </div>
                <div className="form-group">
                    <label>Image</label>
                    <input 
                        type="file" 
                        className="form-control" 
                        placeholder="Input field" />
                </div>
                <div className="form-group">
                    <label>Category</label>
                    
                    <select name="" id="cate" className="form-control" required="required">
                        <option value="Nike">Nike</option>
                        <option value="Balance">Balance</option>
                        <option value="Vans">Vans</option>
                        <option value="Adidas">Adidas</option>
                    </select> 
                </div>
                <button className="btn btn-primary" >Add</button>
                
            </form>
            </div>
            </div>
          );
    }
}

export default Add;
