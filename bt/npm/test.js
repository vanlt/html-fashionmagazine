//Tính tổng sd promise
// var sum = (x, y) => {
//     return new Promise((resolve, rejected) => {

//         if (x > 1) resolve(x + y);
//         rejected(new Error('Dữ liệu nhập và sai'));
//     })
// };
// sum(3, 4)
//     .then(mess => console.log(mess))
//     .catch(mess2 => console.log(mess2));

////Write a function testNum that takes a number as an argument and returns a Promise that tests if the value is less than or greater than the value 10.
// var testNum = (num) => {
//     // Way 1:
//     // return new Promise((resolve, rejects) => {
//     //     if (num > 10) resolve(num + 'is greater than 10, success!');
//     //     rejects(num + 'is less than 10, error!');
//     // })

//     // Way 2:
//     var p = new Promise((resolve, rejects) => {
//         if (num > 10) resolve(num + 'is greater than 10, success!');
//         rejects(num + 'is less than 10, error!');
//     })
//     return p;
// };
// testNum(3)
//     .then(mess => console.log(mess))
//     .catch(err => console.log(err));

// testNum(11)
//     .then(mess => console.log(mess))
//     .catch(err => console.log(err));

//Exercise 2:
//Write two functions that use Promises that you can chain! The first function, makeAllCaps(), 
//will take in an array of words and capitalize them, and then the second function, sortWords(), 
//will sort the words in alphabetical order. If the array contains anything but strings, it should throw an error.

let arr = ['ahihi', 'vanle', 'thuyvan'];

let arrCap = [];
const makeAllCaps = (arr) => {
    return new Promise((resolve, rejects) => {
        let i = 0;

        arr.forEach(element => {
            if (typeof arr[i] === 'string') {
                arrCap.push(arr[i].toUpperCase());
                i += 1;
            } else rejects('Error: Not all items in the array are strings!');
        });
        resolve(arrCap);
    })
}
makeAllCaps(arr)
    .then(mess => console.log(mess))
    .catch(err => console.log(err));

let sortWords = (arr) => {
    return new Promise((resolve, rejects) => {
        let i = 0;
        arr.forEach(element => {
            if (typeof arr[i] !== 'string') {
                rejects('Error: Not all items in the array are strings!');
            }
        });
        resolve(arr.sort());
    })
}

sortWords(arr)
    .then(mess => console.log(mess))
    .catch(err => console.log(err));