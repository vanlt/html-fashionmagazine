import React from 'react';
import axios from 'axios';
import ItemProd from "./Item-prod"

class Product extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            products:[]
        }
      }

    componentWillMount() {
        axios.get("http://172.16.2.79:1494/api/products") 
        .then(response => {
            this.setState({ products: response.data });
            })
            //alert(this.state.Product.name)
        .catch(err => console.log(err));
    };

    
    render() {
        return (
            <React.Fragment>
                <div className="container">
                    {this.state.products.map((item) => (<ItemProd items = {item}/>))} 
                </div>
            </React.Fragment>
        );
    }
}
export default Product;
