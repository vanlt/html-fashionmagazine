import React from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import './App.css';
import ListProds from './components/product/ListProds';
import Login from './components/login/Login';
import Add from './components/admin/product/Add';

class App extends React.Component {
  render(){
    return (
    <Router>
      {/* <React.Fragment>
        <Login/>
        <ListProds/>
      </React.Fragment> */}
      <Link to='/add'>Add</Link>
      <div className="App">
          <Route path="/add" component={Add} /> 
      </div>
    </Router>
    );
  }
  }
export default App;
