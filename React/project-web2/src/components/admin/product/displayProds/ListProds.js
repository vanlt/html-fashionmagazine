import React from 'react';
import axios from 'axios';
import AdminItemProd from "./Item-prod";

class AdminListProds extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            products:[]
        }
      }

    componentWillMount() {
        axios.get("http://localhost:3000/products") 
        .then(response => {
            this.setState({ products: response.data });
            })
        .catch(err => console.log(err));
    };

    
    render() {
        return (
            <React.Fragment>
                <div className="container">
                <table className="table table-hover table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Title</th>
                            <th scope="col">Image</th>
                            <th scope="col">Description</th>
                            <th scope="col">Category</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.products.map((item) => (<AdminItemProd items = {item}/>))}
                    </tbody>
                </table>
                     
                </div>
            </React.Fragment>
        );
    }
}
export default AdminListProds;