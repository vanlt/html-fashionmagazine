import React from 'react';

class ItemProd extends React.Component {
    constructor(props){
        super(props);
      }
    render() {
        return (
            <React.Fragment>
                  
                  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                      <div class="thumbnail">
                          <img src={this.props.items.image} alt=""/>>
                          <div class="caption">
                              <h3>{this.props.items.name}</h3>
                              <p>
                                {this.props.items.description}
                              </p>
                              <p>
                                  <a href="#" class="btn btn-primary">Action</a>
                                  <a href="#" class="btn btn-default">Action</a>
                              </p>
                          </div>
                      </div>
                  </div>
                  
            </React.Fragment>
        );
    }
}
export default ItemProd;
