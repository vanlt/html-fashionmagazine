import React from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import './App.css';
import ListProds from './components/product/ListProds';
import Login from './components/login/Login';
import Add from './components/admin/product/Add';
import AdminItemProd from './components/admin/product/displayProds/ListProds';
class App extends React.Component {
  render(){
    return (
    <Router>
      <React.Fragment>
        {/* <Login/>
        <ListProds/> */}
      <AdminItemProd/>
      </React.Fragment>
      <div className="App">
          <Route exact path="/add" component={Add} /> 
      </div>
    </Router>
    );
  }
  }
export default App;
