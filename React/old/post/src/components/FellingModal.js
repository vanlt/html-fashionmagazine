import React, {Component} from 'react';
import FellingModal from './FellingModal.css';

class Felling_Modal extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            listFelling : [
                {
                    index: 0,
                    title :  'Grinning',
                    img :  require('./img/Grinning.png')
    
                },
                { 
                    index: 1,
                    title :  'Grimacing',
                    img : require('./img/hihi.jpg')
    
                },
                {
                    index: 2,
                    title :  'Grin',
                    img : require('./img/Grin.png')
    
                },
                {
                    index: 3,
                    title :  'Joy',
                    img : require('./img/joy.png')
    
                },
                {
                    index: 4,
                    title :  'Hugging',
                    img : require('./img/huggin.png')
    
                },
                {
                    index: 5,
                    title :  'Scream',
                    img : require('./img/scream.png')
    
                },
                {
                    index: 6,
                    title :  'Broken Heart',
                    img : require('./img/broken-heart.png')
    
                },
            ],

            title : "ahihi",
            img : require('./img/broken-heart.png'),
        };
    }


    getFelling = (index) =>{
        const result = this.state.listFelling.find( item => parseInt(item.index) === index );
        this.setState(
            {title:result.title,
            img:result.img ,
        },
            () => { console.log(this.state.boardAddModalShow)}
        );  
        this.props.handleClickParent(result.title, result.img)
    }
    
    

    render(){
        return (
            <div>
                <button type="button" className="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Felling Option</button>
                <div className="modal fade" id="myModal" role="dialog">
                    <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        <h4 className="modal-title">Felling</h4>
                        <label>You are feeling {this.state.title}
                            <img className="icon" src={this.state.img} alt="hii"/>
                        </label>
                        </div>
                        <div className="modal-body">
                            <div>
                            <table className="table table-hover">
                                <tbody>
                                    {this.state.listFelling.map((value, index) => (
                                        <tr key={index}>
                                            <td>{value.title}</td>
                                            <td><img className="icon" src={value.img} alt="hii"/></td>
                                            <td><button type="button" className="btn btn-default" onClick={() => this.getFelling(index)}>select</button></td>
                                        </tr>
                                    ))} 
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <div className="modal-footer">
                        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
          );
    }
  
}

export default Felling_Modal;


