import React from 'react';
import './App.css';

class postForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            felling: '',
            des: ''
        };

        this.handleChangeFelling = this.handleChangeFelling.bind(this);
        this.handleChangeDes = this.handleChangeDes.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeFelling(event) {
        this.setState({ felling: event.target.value });
    }

    handleChangeDes(event) {
        this.setState({ des: event.target.value });
    }

    handleSubmit(event) { 
        <PostItem /> ;
        this.refs.form.reset();
    }
    render() {
        return (
            //POST
            <div className = "container" >
                <div className = "panel panel-info" >
                    <div className = "panel-heading" >
                        <h3 className = "panel-title" > Post Status </h3> </div> 
                        <div className = "panel-body" >
                        <form ref = "form"onSubmit = { this.handleSubmit } role = "form" >
                        <div className = "form-group" >
                        <div className = "row" >
                        <div className = "col-xs-2 col-sm-2 col-md-2 col-lg-2" >
                        <label > Felling </label>   </div> 
                        <div className = "col-xs-7 col-sm-7 col-md-7 col-lg-7" >
                        <select className = "form-control"
                        value = { this.state.fell }
                        onChange = { this.handleChangeFelling } >
                        <option > Grinning < /option> 
                        <option > Grimacing < /option> 
                        <option > Grin < /option> 
                        <option > Joy < /option> 
                        <option > Hugging < /option> 
                        <option > Scream < /option> 
                        <option > Broken Heart < /option> 
                        </select> 
                        </div> 
                        </div> 
                        </div> 
                        <div className = "form-group" >
                        <div className = "row" >
                        <div className = "col-xs-2 col-sm-2 col-md-2 col-lg-2" >

                        </div> 
                        <div className = "col-xs-7 col-sm-7 col-md-7 col-lg-7" >
                        <div className = "md-form" >
                        <textarea id = "form10"
                        className = "md-textarea form-control"
                        rows = "3"
                        placeholder = "write something here"
                        value = { this.state.des }
                        onChange = { this.handleChangeDes } > </textarea> 
                        </div> 
                        </div> 
                        </div> 
                        </div> 
                        <button type = "submit"
                        className = "btn btn-primary"
                        value = "Submit" > Post </button> 
                        </form> 
                    </div> 
                </div> 
            </div>
        );
    }
}

class PostItem extends React.Component {

    render() {
        return ( <
            div className = "row" >
            <
            div className = "col-xs-6 col-sm-6 col-md-6 col-lg-6" >
            <
            div className = "media" >
            <
            a className = "pull-left"
            href = "#" >
            <
            img className = "media-object"
            src = "https://i.pinimg.com/originals/2f/d5/27/2fd5270489ad5c63fdda63ff9b01038b.jpg"
            alt = "Image" / >
            <
            /a> <
            div className = "media-body" >
            <
            h4 className = "media-heading" > You are feeling { this.props.felling } < /h4> <
            p > { this.props.des } < /p> <
            /div> <
            /div> <
            /div> <
            /div>
        );
    }
}

export default postForm;