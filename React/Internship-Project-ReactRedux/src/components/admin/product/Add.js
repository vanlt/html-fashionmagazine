import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import addProd from "../../../store/action/addProdsAction";

class Add extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            img: "hihi",
            category: ""
        }
    }

    componentWillMount() {
        //-- call API server, get all posts

    }

    render(){
        return (
            <div className="container">
            <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <form>
                <legend>Add New Product</legend>
                <div className="form-group">
                    <label>Title</label>
                    <input 
                        type="text" 
                        className="form-control"  
                        placeholder="Input field" 
                        value={this.state.value}
                        onChange={(event)=>{this.setState({title:event.target.value})}}/>
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        placeholder="Input field" value={this.state.value}
                        onChange={(event)=>{this.setState({description:event.target.value})}}/>
                </div>
                <div className="form-group">
                    <label>Image</label>
                    <input 
                        type="file" 
                        className="form-control" 
                        placeholder="Input field" />
                </div>
                <div className="form-group">
                    <label>Category</label>
                    <select className="form-control" required="required" value={this.state.value}
                        onChange={(event)=>{this.setState({category:event.target.value})}}>
                        <option value="Nike">Nike</option>
                        <option value="Balance">Balance</option>
                        <option value="Vans">Vans</option>
                        <option value="Adidas">Adidas</option>
                    </select> 
                </div>
                <button className="btn btn-primary" onClick={() => {
                   this.props.addProd(this.state.title, this.state.img, this.state.description, this.state.category)
                }} >Add</button>
                
            </form>
            </div>
            </div>
          );
    }
}

const mapStateToProps = (store) => {
    return {
        prodInfor: store.prodReducers.prods,
    };
};

const mapDispatchToProps = (dispactch) => {
    return bindActionCreators({
        addProd
    }, dispactch);
};
export default connect(mapStateToProps,mapDispatchToProps)(Add);
