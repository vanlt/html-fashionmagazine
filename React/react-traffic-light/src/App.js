import React from 'react';
import './App.css';
import TrafficLight from './components/Traffic_light';

class App extends React.Component{
  render(){
    return(
      <div className="App">
        <TrafficLight />
      </div>
    );
  }
}

export default App;
