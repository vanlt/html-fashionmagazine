import React from 'react';

class Login extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return ( <
            div className = "container" >
            <
            div className = "col-xs-5 col-sm-5 col-md-5 col-lg-5" >
            <
            form >
            <
            legend > Login < /legend> <
            div className = "form-group" >
            <
            label > Username < /label> <
            input type = "text"
            className = "form-control"
            placeholder = "Input field" / >
            <
            /div> <
            div className = "form-group" >
            <
            label > Password < /label> <
            input type = "password"
            className = "form-control"
            placeholder = "Input field" / >
            <
            /div> <
            button className = "btn btn-primary" > Login < /button>

            <
            /form> <
            /div> <
            /div>
        );
    }
}

export default Login;