import React from 'react';
import './productstyle.css';    

class AdminItemProd extends React.Component {
    constructor(props){
        super(props);
      }
    render() {
        return (
            <React.Fragment>
                <tr key={this.props.items.id}>
                        <td>{this.props.items.title}</td>
                        <td><img className="img" src={this.props.items.img} alt="" /></td>
                        <td>{this.props.items.description}</td>
                        <td>{this.props.items.category}</td>
                        <td>
                            <a href="#" className="btn btn-primary">Update</a>
                            <a href="#" className="btn btn-default">Delete</a>
                        </td>
                 </tr>
            </React.Fragment>
        );
    }
}
export default AdminItemProd;
