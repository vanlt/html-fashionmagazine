import React from 'react';
import './Message.css';

const RED = 0;
class MessageTf extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            currentMessage : RED
        };
    }

    render(){
        const {currentMessage} = this.props;
        let className = "";
        let message = "";
        if(currentMessage === 0) {className = "red"; message = "STOP"}
        if(currentMessage === 1) {className = "orange"; message = "WAIT"}
        if(currentMessage === 2) {className = "green"; message = "GO"}
        return(
        <div className='Message'>
            <div className={className}>{message}</div>
        </div>
        
        );
    }
}

export default MessageTf;
