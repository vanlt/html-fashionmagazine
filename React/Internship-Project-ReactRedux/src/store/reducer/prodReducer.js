const initialState = {
    prods: [],
    action: ''
};

const prodReducers = (state = initialState, action) => {
    state = {...state, lastAction: action.type };

    switch (action.type) {
        case "ADD":
            return {
                ...state,
                prods: [
                    ...state.prods,
                    { title: action.data.title, img: action.data.img, description: action.data.description, category: action.data.category }
                ]
            };

        default:
            return state;
    }
}

export default prodReducers;