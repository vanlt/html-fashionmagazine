import React from 'react';
import './DisplayFelling.css';

class DisplayFelling extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        alert("hihi"+ this.props.content) 
        return (
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object" id="img" src={require('./img/logo.png')} alt="Image" />
                </a>
                <div class="media-body">
                    <h4 class="media-heading">You're felling {this.props.title} </h4>
                    <p>{this.props.content} </p>
                </div>
            </div>
          );
    }
  
}
export default DisplayFelling;
