import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";

import { prodReducers } from "./reducer/prodReducer";

const configStore = () => {
    return createStore(
        combineReducers({
            prodReducers: prodReducers,
        }),
        applyMiddleware(thunk)
    );
};

export {configStore};