import React from 'react';
import './Item.css';

class ItemProd extends React.Component {
    constructor(props){
        super(props);
      }
    render() {
        return (
            <React.Fragment>
                  <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="thumbnail">
                          <img className="img" src={this.props.items.image} alt=""/>
                          <div className="caption">
                              <h3>{this.props.items.name}</h3>
                              <p className= "des">
                                {this.props.items.description}
                              </p>
                              <p>
                                  <a href="#" className="btn btn-primary">Action</a>
                                  <a href="#" className="btn btn-default">Action</a>
                              </p>
                          </div>
                      </div>
                  </div>
            </React.Fragment>
        );
    }
}
export default ItemProd;
