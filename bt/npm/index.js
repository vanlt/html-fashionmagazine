var express = require("express");//ExpressJS là một Web Application Framework, dòng code này nói rằng bạn muốn sử dụng nó.
 
var app = express();//Tạo một đối tượng Express.
 
app.use(express.static("public"));//Dòng code này nói với Application Server rằng bạn muốn sử dụng thư mục public để chứa các dữ liệu tĩnh, người dùng có thể truy cập vào các file trong thư mục này.
 
app.set("view engine", "ejs");//Dòng code này nói với Application Server rằng bạn muốn sử dụng thư viện EJS, nó là bộ máy xử lý các trang của bạn. EJS sẽ tạo ra HTML trả về phía trình duyệt của người dùng.
app.set("views", "./views");//Dòng code này chỉ cho Application Server đường dẫn tới thư mục chứa các trang của bạn
 
app.listen(3000);
 
app.get("/index", function(request, response)  {
    
    response.render("index");
});

