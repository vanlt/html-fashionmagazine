import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import './App.css';
import Home from './components/Home';

class App extends React.Component{
  render(){
    return (
      <Router>
        <React.Fragment>
          <ul>
            <li><Link to= "/">Home</Link></li>
            <li><Link to= "/new">New</Link></li>
            <li><Link to= "/about">About</Link></li>
            <li><Link to= "/contact">Contact</Link></li>
          </ul>
          <Route exact path ="/" component={Home}/>
          <Route  path ="/new" component={New}/>
          <Route  path ="/about" component={About}/>
          <Route  path ="/contact" component={Contact}/>
        </React.Fragment>
      </Router>
    );
  }
  
}

export default App;
 