import React from 'react';
import Felling_Modal from './FellingModal';
import DisplayFelling from './DisplayFelling';

class Form_post extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            title : "ahihi",
            img : require('./img/broken-heart.png'),
        }
       
        this.content = React.createRef();
    }

    callbackFelling = (title, img) => {
        this.setState({
            title : title,
            img : img,
        },
            () => { console.log(this.state.boardAddModalShow)})
    }

    handleClickPost = (e) => {     
        e.preventDefault();
    }

    render(){
        return (
            <div className="form">
            <div className="container" >
                <div className="panel panel-info">
                    <div className="panel-heading">
                    <h3 className="panel-title">Post Status</h3>
                    </div>
                    <div className="panel-body"> 
                    <form onSubmit= {() =>this.handleClickPost()}>
                        <div className="form-group">
                                <div className="row">
                                    <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                        <Felling_Modal handleClickParent={this.callbackFelling} />
                                    </div>
                                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <div className="md-form">
                                    <label>You are feeling {this.state.title}
                                        <img className="icon" src={this.state.img} alt="hii"/>
                                    </label>
                                    </div>
                                    </div>
                                    <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    <button type="submit" className="btn btn-warning"  >Post</button>
                                    </div>
                                </div>
                        </div>
                        <div className="form-group">
                            <div className="row">
                                
                                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div className="md-form">
                                    <textarea 
                                    id="form10" 
                                    className="md-textarea form-control" 
                                    rows="5" 
                                    placeholder="write something here"
                                    ref={(t) => { this.content = t }}></textarea>
                                </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                <DisplayFelling title = {this.state.title} img = {this.state.img} content={this.content.value}/>
                {alert(this.content.value)}
            </div> 
            </div>
        );
    }
  
}

export default Form_post; 